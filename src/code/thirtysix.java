package code;

import java.util.Random;
import java.util.Scanner;

public class thirtysix {
	public static void main(String[] args) {

		
		Scanner input = new Scanner(System.in);
		Random rand = new Random();
		  int total = 0;
		  int rollCount = 0;
	  
	  System.out.println("Bienvenue au jeu de d�s Trente-Six!");
	  
	  while (total != 36 && total != 7 && total != 11 && total != 2 && total != 3 && total != 12) {
	     System.out.print("Appuyez sur 'Entr�e' pour lancer les d�s.");
	     input.nextLine();
	     
	     int roll1 = rand.nextInt(6) + 1;
	     int roll2 = rand.nextInt(6) + 1;
	     int rollTotal = roll1 + roll2;
	     
	     System.out.println("Vous avez lanc� " + roll1 + " et " + roll2 + " pour un total de " + rollTotal + ".");
	     
	     if (rollTotal == 7 || rollTotal == 11) {
	        System.out.println("Vous avez gagn�!");
	        break;
	     }
	     else if (rollTotal == 2 || rollTotal == 3 || rollTotal == 12) {
	        System.out.println("Vous avez perdu.");
	        break;
	     }
	     
	     total += rollTotal;
	     rollCount++;
	     
	     System.out.println("Votre total est maintenant " + total + ".");
	  }
	  
	  if (total == 36) {
	     System.out.println("F�licitations! Vous avez atteint le total de 36 en " + rollCount + " lancers!");
	  }
	}
}
